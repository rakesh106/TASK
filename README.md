This repo contains Simple APIs to create and fetch Users

Request path in folders:
  starts with index.js->startup-routes.js->routes-user.router.js->controller-user.controller.js

1.API to create a user:
  POST-'https://task-user-apis.herokuapp.com/users/create-user'

  sample req body:
  {
      "name":"Ryan Garcia",
      "age":25
  }
  sample response body:
  {
    "Success": true,
    "code": 200,
    "message": "User created successfully"
  }

  Both Name and age fields are required, Names are case-sensitive and same combination of name+age is regarded as a duplicate.

2.API to get user by name:

  GET-'https://task-user-apis.herokuapp.com/users/get/:name'

  Name should be passed as a request param and is case sensitive

  sample response body:
  {
    "Success": true,
    "code": 200,
    "data": [
        {
            "userId": "24b2681d-7908-50b3-9440-50a96f27f7d6",
            "name": "Virat Kohli",
            "age": 35
        }
    ]
  }

3.API to get All Users:

  GET-'https://task-user-apis.herokuapp.com/users/get-all-users?pageNumber=2&pageSize=2&sortOrder=asc'

  It supports Pagination

  Request query params:

   1.PageSize should be a number, not a required field and default value will be 10.

   2.PageNumber should be a number, not a required field and default value will 1.
   
   3.SOrtOrder either should be 'asc' or 'desc' and default value will be 'asc'

   sample response body:
   {
    "Success": true,
    "code": 200,
    "data": {
        "totalNoOfUsers": 6,
        "totalPages": 3,
        "currentPage": 2,
        "pageSize": 2,
        "users": [
            {
                "userId": "a9b6fe21-3e84-5b2d-8143-f762742b6937",
                "name": "Ryan Garcia",
                "age": 25
            },
            {
                "userId": "f652d7ae-484b-59ea-a4d8-4daaa7675a9f",
                "name": "KL Rahul",
                "age": 31
            }
        ],
        "links": {
            "next": "task-user-apis.herokuapp.com/users/get-all-users?pageNumber=3&pageSize=2&sortOrder=asc",
            "previous": "task-user-apis.herokuapp.com/users/get-all-users?pageNumber=1&pageSize=2&sortOrder=asc"
        }
    }
   }
  
